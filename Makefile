MAJORVERSION=0
MINORVERSION=1

LIBDIR=/usr/lib
INCDIR=/usr/include
OPTFLAGS=-march=native -mtune=native -O3
CFLAGS=-std=c99 -pedantic -Wall -Wextra ${OPTFLAGS}
LIBS=-lm
LDFLAGS=-fpic ${LIBS}

all: lib/libcerebellum.so.${MAJORVERSION}.${MINORVERSION} lib/libcerebellum.a

install: lib/libcerebellum.so.${MAJORVERSION}.${MINORVERSION} uninstall
	cp include/cerebellum.h ${INCDIR}
	cp $< ${LIBDIR}
	ln -s ${LIBDIR}/libcerebellum.so.${MAJORVERSION}.${MINORVERSION} ${LIBDIR}/libcerebellum.so.${MAJORVERSION}
	ln -s ${LIBDIR}/libcerebellum.so.${MAJORVERSION} ${LIBDIR}/libcerebellum.so

lib/libcerebellum.so.${MAJORVERSION}.${MINORVERSION}: matrix.o learning.o
	mkdir lib || echo continue
	cc ${LDFLAGS} -shared -o $@ $^ -Wl,-soname,libcerebellum.so.${MAJORVERSION}

lib/libcerebellum.a: matrix.o learning.o
	mkdir lib || echo continue
	ar rcs $@ $^

%.o: src/%.c
	cc ${CFLAGS} ${LDFLAGS} -I include -c -o $@ $<

uninstall:
	rm -f ${INCDIR}/cerebellum.h
	rm -f ${LIBDIR}/libcerebellum.*
